import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Books } from '../shared/models/books.interface';

@Injectable({
    providedIn: 'root'
})
export class booksService {
    headersObject: HttpHeaders;
    httpOptions: any;
    constructor(private http: HttpClient, private toastr: ToastrService) {

    }
    listarbooksAvailable(): Observable<Books[]> {

        return this.http.get(`${environment.URL_BACKEND}/book/available`).pipe(
            map((res: any) => {
                const books: Books[] = res.response;
                return books;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }
    listarbooks(data: any): Observable<Books[]> {
        let params;
        if (data && data.book_id) {
            params = new HttpParams().set('book_id', data.book_id);
        }
        return this.http.get(`${environment.URL_BACKEND}/book`, { params }).pipe(
            map((res: any) => {
                const books: Books[] = res.response;
                return books;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    updatebooks(data: any): Observable<any> {
        return this.http.put(`${environment.URL_BACKEND}/book/${data.id}`, data).pipe(
            map((res: any) => {
                this.toastr.success(res.message);
                return res;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    createbooks(data: any): Observable<any> {
        return this.http.post(`${environment.URL_BACKEND}/book`, data).pipe(
            map((res: any) => {
                this.toastr.success(res.message);
                return res;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    handleError(err: any): Observable<never> {
        if (err) {
            let errorMessage: any;
            if (typeof err.error.error === 'object') {
                errorMessage = err.error.error.message;
                this.toastr.error(err.error.error.message);
            } else {
                errorMessage = err.error.message;
                this.toastr.error(err.error.message);
            }
            return errorMessage;
        }
        return err;
    }
}
