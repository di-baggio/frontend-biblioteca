import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { take } from 'rxjs/operators';
import { booksService } from './books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  listLibros: any[] = [];
  listEstados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridLibros', { static: true }) gridUsuario: DxDataGridComponent;
  @ViewChild('ModalActualizarEditar', { static: true }) modal: ElementRef;

  isNew = false;
  form = this.fb.group({
    id: [0],
    code: ['', [Validators.required]],
    nombre: ['', [Validators.required]],
    quantity: ['', [Validators.required]],
    url_image: [''],
    state: [1, [Validators.required]]
  });
  constructor(private bookService: booksService, private fb: FormBuilder) {
    this.editar = this.editar.bind(this);
    this.desactivar = this.desactivar.bind(this);
  }

  async ngOnInit() {
    this.listarBooks();
  }
  listarBooks() {
    this.bookService.listarbooks({}).pipe(take(1)).subscribe((res: any) => {
      if (res) {
        this.listLibros = res;
      }
    });
  }

  editar(e) {
    this.form.patchValue(e.row.data);
    this.abrirModal(false);
  }
  desactivar(e) {
    this.form.patchValue(e.row.data);
    this.form.get('state').setValue(0);
    this.onCreateBook();
  }

  abrirModal(isNew: boolean) {
    this.isNew = isNew;
    this.modal.nativeElement.className = "modal fade show";
    this.modal.nativeElement.setAttribute('style', "display:block");
  }
  cancelarModal() {
    this.modal.nativeElement.className = "modal fade hide";
    this.modal.nativeElement.setAttribute('style', "display:none");
    this.limpiarForm();
  }
  onCreateBook() {
    const formValue = this.form.value;

    if (this.isNew) {
      this.bookService.createbooks(formValue).pipe(take(1)).subscribe(async res => {
        this.listarBooks();
      });
    } else {
      this.bookService.updatebooks(formValue).pipe(take(1)).subscribe(async res => {
        this.listarBooks();
      });
    }

    this.cancelarModal();
    this.limpiarForm();
  }
  limpiarForm() {
    this.form.reset();
  }
}
