export interface Users {
    identification_number: number;
    name: string;
    email: string;
    password: string;
    state: number;
}
