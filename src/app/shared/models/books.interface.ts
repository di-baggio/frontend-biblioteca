export interface Books {
    code: string,
    nombre: string,
    quantity: number,
    url_image: string,
    state: number
}