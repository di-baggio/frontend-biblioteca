import { Books } from "./books.interface";
import { Users } from "./Users.interface";

export interface BorrowedBooks {
    user_id: number;
    book_id: number;
    borrowed_date: Date;
    return_date: Date;
    user: Users;
    book: Books;
}