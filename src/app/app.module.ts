import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { UsersComponent } from './users/users.component';
import { BorrowedBooksComponent } from './borrowed-books/borrowed-books.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';

import {
  DxDataGridModule,
  DxButtonModule,
  DxSwitchModule,
  DxSelectBoxModule,
  DxDateBoxModule,
} from 'devextreme-angular';
import { UsersService } from './users/users.service';


@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    UsersComponent,
    BorrowedBooksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    DxDataGridModule,
    DxButtonModule,
    DxSwitchModule,
    DxSelectBoxModule,
    DxDateBoxModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
