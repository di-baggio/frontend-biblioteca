import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BorrowedBooks } from '../shared/models/borrowed-books.interface';

@Injectable({
    providedIn: 'root'
})
export class BorrowedBooksService {
    headersObject: HttpHeaders;
    httpOptions: any;
    constructor(private http: HttpClient, private toastr: ToastrService) {

    }
    listarBorrowedBooks(data: any): Observable<BorrowedBooks[]> {
        return this.http.get(`${environment.URL_BACKEND}/borrowed`).pipe(
            map((res: any) => {
                const borrowedBooks: BorrowedBooks[] = res.response;
                return borrowedBooks;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    updateBorrowedBooks(data: any): Observable<any> {
        return this.http.put(`${environment.URL_BACKEND}/borrowed/${data.id}`, data).pipe(
            map((res: any) => {
                this.toastr.success(res.message);
                return res;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    createBorrowedBooks(data: any): Observable<any> {
        return this.http.post(`${environment.URL_BACKEND}/borrowed`, data).pipe(
            map((res: any) => {
                this.toastr.success(res.message);
                return res;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    handleError(err: any): Observable<never> {
        if (err) {
            let errorMessage: any;
            if (typeof err.error.error === 'object') {
                errorMessage = err.error.error.message;
                this.toastr.error(err.error.error.message);
            } else {
                errorMessage = err.error.message;
                this.toastr.error(err.error.message);
            }
            return errorMessage;
        }
        return err;
    }
}
