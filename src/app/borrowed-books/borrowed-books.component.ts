import { formatDate } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { take } from 'rxjs/operators';
import { booksService } from '../books/books.service';
import { UsersService } from '../users/users.service';
import { BorrowedBooksService } from './borrowed-books.service';

@Component({
  selector: 'app-borrowed-books',
  templateUrl: './borrowed-books.component.html',
  styleUrls: ['./borrowed-books.component.scss']
})
export class BorrowedBooksComponent implements OnInit {

  listPrestamos: any[] = [];
  listUser: any[] = [];
  listBooks: any[] = [];
  listEstados: any[] = [{ estado: false, nombre: 'INACTIVO' }, { estado: true, nombre: 'ACTIVO' }];

  @ViewChild('gridPrestamos', { static: true }) gridUsuario: DxDataGridComponent;
  @ViewChild('ModalActualizarEditar', { static: true }) modal: ElementRef;

  isNew = false;
  form = this.fb.group({
    id: [0],
    user_id: ['', [Validators.required]],
    books_id: ['', [Validators.required]],
    borrowed_date: [formatDate(new Date(), 'yyyy-MM-dd', 'en'), [Validators.required]],
    return_date: [null],
    state: [true, [Validators.required]]

  });
  constructor(private borrowedBooksService: BorrowedBooksService, private bookService: booksService,
    private userService: UsersService, private fb: FormBuilder) {
    this.editar = this.editar.bind(this);
    this.desactivar = this.desactivar.bind(this);
  }

  async ngOnInit() {
    this.listarBooks();
    this.listarUsers();
    this.listarLibros();
  }
  listarLibros() {
    this.bookService.listarbooksAvailable().pipe(take(1)).subscribe((res: any) => {
      if (res) {
        this.listBooks = res;
      }
    });
  }
  listarUsers() {
    this.userService.listarUsers({ state: 1 }).pipe(take(1)).subscribe((res: any) => {
      if (res) {
        this.listUser = res;
      }
    });
  }
  listarBooks() {
    this.borrowedBooksService.listarBorrowedBooks({}).pipe(take(1)).subscribe((res: any) => {
      if (res) {
        this.listPrestamos = res;
      }
    });
  }

  editar(e) {

    this.form.patchValue(e.row.data);
    this.form.get('user_id').disable();
    this.form.get('books_id').disable();
    this.form.get('borrowed_date').setValue(formatDate(e.row.data.borrowed_date, 'yyyy-MM-dd', 'en'));
    this.form.get('return_date').setValue(formatDate(e.row.data.return_date, 'yyyy-MM-dd', 'en'));
    this.form.get('borrowed_date').disable();
    if (!e.row.data.state) {
      this.form.get('state').disable();
      this.form.get('return_date').disable();
    } else {
      this.form.get('state').enable();
      this.form.get('return_date').enable();
    }
    this.abrirModal(false);
  }
  desactivar(e) {
    this.form.patchValue(e.row.data);
    this.form.get('state').setValue(false);
    this.isNew = false;
    this.onCreateBook();
  }

  abrirModal(isNew: boolean) {
    this.isNew = isNew;
    if (isNew) {
      this.form.get('user_id').enable();
      this.form.get('books_id').enable();
      this.form.get('borrowed_date').enable();
      this.form.get('state').enable();
      this.form.get('return_date').enable();
    }
    this.modal.nativeElement.className = "modal fade show";
    this.modal.nativeElement.setAttribute('style', "display:block");
  }
  cancelarModal() {
    this.modal.nativeElement.className = "modal fade hide";
    this.modal.nativeElement.setAttribute('style', "display:none");
    this.limpiarForm();
  }
  onCreateBook() {
    const formValue = this.form.value;

    if (this.isNew) {
      this.borrowedBooksService.createBorrowedBooks(formValue).pipe(take(1)).subscribe(async res => {
        this.listarBooks();
      });
    } else {
      this.borrowedBooksService.updateBorrowedBooks(formValue).pipe(take(1)).subscribe(async res => {
        this.listarBooks();
      });
    }

    this.cancelarModal();
    this.limpiarForm();
    this.listarUsers();
    this.listarLibros();
  }
  limpiarForm() {
    this.form.reset();
  }

}
