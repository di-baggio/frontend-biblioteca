import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { take } from 'rxjs/operators';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  listUsuario: any[] = [];
  listTipoDocumento: any[] = [];

  listEstados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridUsuario', { static: true }) gridUsuario: DxDataGridComponent;
  @ViewChild('ModalActualizarEditar', { static: true }) modal: ElementRef;

  isNew = false;
  UsuarioForm = this.fb.group({
    id: [0],
    identification_number: ['', [Validators.required]],
    name: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
    state: [1, [Validators.required]]
  });
  constructor(private usuarioService: UsersService, private fb: FormBuilder) {
    this.editar = this.editar.bind(this);
    this.desactivarUser = this.desactivarUser.bind(this);
  }

  async ngOnInit() {
    this.listarUsuario();
  }
  listarUsuario() {
    this.usuarioService.listarUsers({}).pipe(take(1)).subscribe((res: any) => {
      if (res) {
        this.listUsuario = res;
      }
    });
  }

  editar(e) {
    this.UsuarioForm.patchValue(e.row.data);
    this.abrirModal(false);
  }
  desactivarUser(e) {
    this.UsuarioForm.patchValue(e.row.data);
    this.UsuarioForm.get('state').setValue(0);
    this.onCreateUsuario();
  }

  abrirModal(isNew: boolean) {
    this.isNew = isNew;
    this.modal.nativeElement.className = "modal fade show";
    this.modal.nativeElement.setAttribute('style', "display:block");
  }
  cancelarModal() {
    this.modal.nativeElement.className = "modal fade hide";
    this.modal.nativeElement.setAttribute('style', "display:none");
    this.limpiarForm();
  }
  onCreateUsuario() {
    const formValue = this.UsuarioForm.value;

    if (this.isNew) {
      this.usuarioService.createUsers(formValue).pipe(take(1)).subscribe(async res => {
        this.listarUsuario();
      });
    } else {
      this.usuarioService.updateUsers(formValue).pipe(take(1)).subscribe(async res => {
        this.listarUsuario();
      });
    }
    this.cancelarModal();
    this.limpiarForm();
  }
  limpiarForm() {
    this.UsuarioForm.reset();
  }

}
