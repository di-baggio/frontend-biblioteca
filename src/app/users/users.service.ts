import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Users } from '../shared/models/Users.interface';

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    headersObject: HttpHeaders;
    httpOptions: any;
    constructor(private http: HttpClient, private toastr: ToastrService) {

    }
    listarUsers(data: any): Observable<Users[]> {
        let params = new HttpParams();
        if (data && (data.user_id || data.state)) {
            if (data.user_id) {
                params = params.set('book_id', data.user_id);
            }
            if (data.state !== undefined) {
                params = params.set('state', data.state);
            }
        }
        return this.http.get(`${environment.URL_BACKEND}/user`, { params }).pipe(
            map((res: any) => {
                const Users: Users[] = res.response;
                return Users;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    updateUsers(data: any): Observable<any> {
        return this.http.put(`${environment.URL_BACKEND}/user/${data.id}`, data).pipe(
            map((res: any) => {
                this.toastr.success(res.message);
                return res;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    createUsers(data: any): Observable<any> {
        return this.http.post(`${environment.URL_BACKEND}/user`, data).pipe(
            map((res: any) => {
                this.toastr.success(res.message);
                return res;
            }),
            catchError((err: any) => this.handleError(err))
        );
    }

    handleError(err: any): Observable<never> {
        if (err) {
            let errorMessage: any;
            if (typeof err.error.error === 'object') {
                errorMessage = err.error.error.message;
                this.toastr.error(err.error.error.message);
            } else {
                errorMessage = err.error.message;
                this.toastr.error(err.error.message);
            }
            return errorMessage;
        }
        return err;
    }
}
