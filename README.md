
Version de angular (8.3.21)
## Pasos para ejecutar el proyecto:

### Paso 1: instalar dependencias

Ejecute npm i en la terminal 

### Paso 2: configuración de variables
Dírigase a la ruta src/environments y edite el archivo environments.ts la propiedad "URL_BACKEND" y coloque la URL donde el backend está esperando por peticiones

### Paso 3: Ejecución del proyecto:
En la terminal ejecute ng serve para correr el proyecto localmente 
